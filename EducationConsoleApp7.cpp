#include <iostream>

class Animal
{
public:
virtual void Voice() {
    std::cout << "Make some noise...";
};
};

class Dog : public Animal
{
public:
void Voice() override
{
    std::cout << "Woof!\n";
}
};

class Cat : public Animal
{
public:
void Voice() override
{
    std::cout << "Meow!\n";
}
};

class Owl : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Hoot!\n";
    }
};

int main()
{
    Animal* AnimalsCollection[3];
    AnimalsCollection[0] = new Cat();
    AnimalsCollection[1] = new Dog();
    AnimalsCollection[2] = new Owl();

    for (Animal* i : AnimalsCollection) {
        i->Voice();
    }
};